</div>
<footer id="footer" role="contentinfo">
    <div class="col-12 col-md-6 footer-info">
        <div class="company-info">
            <a href="/" class="logo">
                <img src="/drbouw/wp-content/uploads/2022/01/drbouw-logo.png" alt="logo" />
            </a>
            <ul>
                <li>
                    <h2>DR Bouw Putten</h2>
                </li>
                <li>
                    <a href="tel:0610608232">06 - 10 60 82 32</a>
                </li>
                <li>
                    <a href="mailto:dean@drbouw.nl">dean@drbouw.nl</a>
                </li>
            </ul>
        </div>
        <?php
            echo do_shortcode('[contact-form-7 id="5" title="Contactformulier 1"]');
        ?>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
</body>
</html>