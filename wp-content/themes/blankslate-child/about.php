<?php
/* Template Name: Over ons */ 
$projectImage = get_field('pagina_afbeelding');
$titel = get_field('pagina_titel');
$subtitel = get_field('pagina_subtitel');
$intro = get_field('pagina_text');
?>

<?php get_header(); ?>
<main id="content" role="main">
    <article>
        <?php if($titel) : ?>
            <section class="section intro-section" <?php if($projectImage) : echo 'style="background-image: url(' . $projectImage . ')"'; endif;?>>
                <div class="col-12 d-flex align-items-center justify-content-between about-wrapper">
                    <div class="title-holder left col-12 col-md-8">
                        <?php 
                            echo '<h1>' . $titel . '</h1>';
                        ?>
                        <?php 
                            if($subtitel) :
                                echo '<h4>' . $subtitel . '</h4>';
                            endif;
                        ?>
                        <?php 
                            if($intro) :
                                echo '<p>' . $intro . '</p>';
                            endif;
                        ?>
                    </div>
                    <div class="about-image-holder">
                        <img src="/drbouw/wp-content/uploads/1517451354109.jpg" alt="dean drbouw" />
                    </div>
                </div>
            </section>
        <?php endif; ?>
    </article>
</main>
<?php get_footer(); ?>