	
<?php 
/* Template Name: Homepage */ 

$bgImage = get_field('achtergrond_afbeelding');
$koptitel = get_field('koptitel');
$introText = get_field('intro_tekst');
$introButtonLink = get_field('intro_knop_link');
$introButtonText = get_field('intro_knop_text');
$instagramTitel = get_field('instagram_titel');
$instagramText = get_field('instagram_text');
$projectTitel = get_field('project_titel');
$projectText = get_field('project_text');
$projectOne = get_field('uitgelicht_project_1');
$projectTwo = get_field('uitgelicht_project_2');
$projectThree = get_field('uitgelicht_project_3');
?>

<?php get_header(); ?>
<main id="content" role="main">
    <article class="homepage custom-page">
        <section class="section intro-section d-flex align-items-center" <?php if($bgImage) : echo 'style="background-image: url(' . $bgImage . ')"'; endif;?>>
            <div class="title-holder col-12 col-md-8">
                <?php 
                    if($koptitel) :
                        echo '<h1>' . $koptitel . '</h1>';
                    endif;
                ?>
                <?php 
                    if($introText) :
                        echo '<p>' . $introText . '</p>';
                    endif;
                ?>
                <?php 
                    if($introButtonLink && $introButtonText) :
                        echo '<a class="btn btn-primary" href="' . $introButtonLink . '" target="_blank">' . $introButtonText . '</a>';
                    endif;
                ?>
            </div>
        </section>
        <section class="section section-projects">
            <div class="col-12">
            <div class="title-holder">
                    <?php 
                        if($projectTitel) :
                            echo '<h2>' . $projectTitel . '</h2>';
                        endif;
                    ?>
                    <?php 
                        if($projectText) :
                            echo '<p>' . $projectText . '</p>';
                        endif;
                    ?>
                </div>
                <div class="project-wrapper d-flex justify-content-between flex-wrap col-12">
                    <?php if($projectOne): ?>
                        <div class="project project-1 col-12 col-md-4" <?php if($projectOne['project_afbeelding']) : echo 'style="background-image: url(' . $projectOne['project_afbeelding']['url'] . ')"'; endif;?>>
                            <a href="<?php echo $projectOne['project_link']; ?> "></a>
                            <div class="project-inner">
                                <h3>
                                    <?php echo $projectOne['project_titel']; ?>    
                                </h3>
                                <p>
                                    <?php echo $projectOne['project_beschrijving']; ?>
                                </p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($projectTwo): ?>
                        <div class="project project-1 col-12 col-md-4" <?php if($projectTwo['project_afbeelding']) : echo 'style="background-image: url(' . $projectTwo['project_afbeelding']['url'] . ')"'; endif;?>>
                            <a href="<?php echo $projectTwo['project_link']; ?> "></a>
                            <div class="project-inner">
                                <h3>
                                    <?php echo $projectTwo['project_titel']; ?>    
                                </h3>
                                <p>
                                    <?php echo $projectTwo['project_beschrijving']; ?>
                                </p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($projectThree): ?>
                        <div class="project project-1 col-12 col-md-4" <?php if($projectThree['project_afbeelding']) : echo 'style="background-image: url(' . $projectThree['project_afbeelding']['url'] . ')"'; endif;?>>
                            <a href="<?php echo $projectThree['project_link']; ?> "></a>
                            <div class="project-inner">
                                <h3>
                                    <?php echo $projectThree['project_titel']; ?>    
                                </h3>
                                <p>
                                    <?php echo $projectThree['project_beschrijving']; ?>
                                </p>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section class="section section-instagram">
            <div class="col-12">
                <div class="title-holder">
                    <?php 
                        if($instagramTitel) :
                            echo '<h2>' . $instagramTitel . '</h2>';
                        endif;
                    ?>
                    <?php 
                        if($instagramText) :
                            echo '<p>' . $instagramText . '</p>';
                        endif;
                    ?>
                </div>
                <?php echo do_shortcode('[enjoyinstagram_mb]'); ?>
            </div>
        </section>
    </article>
</main>
<?php get_footer(); ?>
