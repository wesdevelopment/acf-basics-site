<?php
/* Template Name: Project Overview */ 
$projectImage = get_field('pagina_afbeelding');
$titel = get_field('pagina_titel');
$subtitel = get_field('pagina_subtitel');
$intro = get_field('pagina_text');
?>

<?php get_header(); ?>
<main id="content" role="main">
    <article>
        <?php if($titel) : ?>
            <section class="section intro-section d-flex align-items-center" <?php if($projectImage) : echo 'style="background-image: url(' . $projectImage . ')"'; endif;?>>
                <div class="title-holder col-12 col-md-8">
                    <?php 
                        echo '<h1>' . $titel . '</h1>';
                    ?>
                    <?php 
                        if($subtitel) :
                            echo '<h4>' . $subtitel . '</h4>';
                        endif;
                    ?>
                    <?php 
                        if($intro) :
                            echo '<p>' . $intro . '</p>';
                        endif;
                    ?>
                </div>
            </section>
        <?php endif; ?>
        <div class="project-overview">
            <?php
                $args = array(
                    'post_type'=> 'post',
                    'orderby'    => 'ID',
                    'post_status' => 'publish',
                    'order'    => 'DESC',
                    'posts_per_page' => -1 // this will retrive all the post that is published 
                );
                $result = new WP_Query( $args );
                if ( $result-> have_posts() ) : 
                    while ( $result->have_posts() ) : $result->the_post();?>
                    <div class="col-12 col-md-4 project-item">
                        <?php if ( has_post_thumbnail() ) : ?>
                            <a href="<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false ); echo esc_url( $src[0] ); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'full', array( 'itemprop' => 'image' ) ); ?></a>
                        <?php endif; ?>
                        <div class="info-holder"><h4><?php the_title(); ?></h4>
                        <?php the_content(); ?></div>
                    </div>
                <?php endwhile;
                endif; 
                wp_reset_postdata();
            ?>
        </div>
    </article>
</main>
<?php get_footer(); ?>
